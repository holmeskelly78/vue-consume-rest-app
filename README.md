# vue-consume-rest-app

## Create project

``` bash
# initiate project
vue init webpack vue-consume-rest-app

# to get started
cd vue-consume-rest-app
npm run dev

# install axios dependencies
npm install vuex --save
npm install axios --save
npm install sass-loader node-sass --save-dev
# install bootstrap dependencies
npm install --save-dev bootstrap-vue
# install json-server dependencies
npm install -g json-server
npm run dev

# deploy api resources
json-server restData.js -p 3500
```

> vuejs with rest services

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
